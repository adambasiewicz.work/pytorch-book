from torchvision import models
import torch
from torchvision import transforms
from PIL import Image
import sys
def main():
    resnet = models.resnet101(weights= 'DEFAULT')
    #print(resnet)
    
    preprocess = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(
    mean=[0.485, 0.456, 0.406],
    std=[0.229, 0.224, 0.225]
    )])
    image = sys.argv[1]
    #print(image)
    img = Image.open(image)
    #print(img)

    img_t = preprocess(img)


    batch_t = torch.unsqueeze(img_t, 0)
    resnet.eval()

    out = resnet(batch_t)

    with open("imagenet_classes.txt") as f:
        labels = [line.strip() for line in f.readlines()]

    _, index = torch.max(out, 1)
    #print(index)

    percentage = torch.nn.functional.softmax(out, dim=1)[0] * 100
    #print(labels[index[0]], percentage[index[0]].item())

    _, indices = torch.sort(out, descending=True)
    print([(labels[idx], percentage[idx].item()) for idx in indices[0][:5]])

if __name__ == "__main__":
    main()
