def main():
    def training_loop(
        n_epochs, optimizer, model, loss_fn, t_u_train, t_u_val, t_c_train, t_c_val
    ):
        for epoch in range(1, n_epochs + 1):
            model.train()  # Set the model to training mode
            t_p_train = model(t_u_train)  # Forward pass on training data
            loss_train = loss_fn(t_p_train, t_c_train)  # Calculate training loss

            optimizer.zero_grad()  # Zero the gradients
            loss_train.backward()  # Backpropagation
            optimizer.step()  # Update the parameters

            model.eval()  # Set the model to evaluation mode
            with torch.no_grad():  # Disable gradient computation
                t_p_val = model(t_u_val)  # Forward pass on validation data
                loss_val = loss_fn(t_p_val, t_c_val)  # Calculate validation loss

            if epoch == 1 or epoch % 500 == 0:  # Print loss every 500 epochs
                print(
                    f"Epoch {epoch}, Training loss {loss_train.item():.4f}, Validation loss {loss_val.item():.4f}"
                )

    # Train the model using the training loop
    training_loop(
        n_epochs=5000,
        optimizer=optimizer,
        model=model,
        loss_fn=loss_fn,
        t_u_train=train_sulfur,
        t_u_val=val_sulfur,
        t_c_train=train_target,
        t_c_val=val_target,
    )


if __name__ == "__main__":
    pass
