import torch


def main():
    tensor = torch.tensor([1, 2, 3, 4, 5, 6, 7])
    print(f"{tensor=}")
    copy = tensor
    print("tensor[2] = 125")
    tensor[2] = 125

    print(f"{tensor=}")
    print(f"{copy=}")
    print(
        "Even though only the original tensor was changed, copy of it was changed also, to avoid it, use .clone() method"
    )

    clone = tensor.clone()

    tensor[5] = 300
    print("tensor[5] = 300")

    print(f"clone == tensor = {clone==tensor}")


if __name__ == "__main__":
    main()
